
# coding: utf-8

# In[1]:


import sys
import json
import time
import os
import numpy as np
import pandas as pd
import math
import random
import csv
import iso8601
import datetime


# In[2]:


#P3: total duration of the session
#BURADA BİRDEN FAZLA OLAN SESSİON'LARIN DURATION'I HESAPLANDI. 
#TEK TIK OLAN SESSIONLAR İÇİN ORTALAMA DURATION DEĞERİNİ ATA
#only-multiples.csv dosyasına sırası ile birden fazla olan sessionlar kondu ki 
#total_duration hesaplanabilsin.
file = path = "/Users/rumeysabulut/Documents/Bitirme/yoochoose-data" + "/train-all.csv"
df = pd.read_csv(filepath_or_buffer = file, header=None, sep=',') 
lines = df.iloc[:,0:4].values
        
lines = lines[lines[:,0].argsort()]

with open("only-multiples.csv", "w") as out:
    for i in range(lines.shape[0]):
        data = lines[i].tolist()
        
        if i == 0:
            if lines[i][0] == lines[i+1][0]:
                writer = csv.writer(out)
                writer.writerow(data)
        
        elif i == lines.shape[0]-1:
            if lines[i][0] == lines[i-1][0]:
                writer = csv.writer(out)
                writer.writerow(data)
        
        elif lines[i][0] == lines[i-1][0] or lines[i][0] == lines[i+1][0]:
            writer = csv.writer(out)
            writer.writerow(data)
            


# In[3]:



f1 = path = "/Users/rumeysabulut/Documents/Bitirme/yoochoose-data" + "/train-all.csv"
df = pd.read_csv(filepath_or_buffer = f1, header=None, sep=',') 
lines = df.iloc[:,0:4].values

#to compute average value for one-clicked sessions
#I need the number of all sessions

all_sess = {}
for i in range(lines.shape[0]):
    if lines[i][0] not in all_sess:
        all_sess[lines[i][0]] = i
        

f2 = path = "/Users/rumeysabulut/Documents/Bitirme/yoochoose-data" + "/only-multiples.csv"
pf = pd.read_csv(filepath_or_buffer = f2, header=None, sep=',') 
lines_multi = pf.iloc[:,0:2].values

dict_multiples = {}

for i in range(lines_multi.shape[0]):    
    inp = lines_multi[i][0]
    val = iso8601.parse_date(lines_multi[i][1])
    if inp in dict_multiples:
        dict_multiples[inp].append(val)
    else:
        dict_multiples[inp] = [val]
              
for key in dict_multiples:
    time_list = []
    time_list = dict_multiples.get(key)

    start = min(time_list)
    end = max(time_list)
    duration = end - start
    dict_multiples[key] = duration

total_val = datetime.datetime.now();
total_val -= total_val;
for key in dict_multiples:
    total_val += dict_multiples.get(key)

avg_duration = total_val / len(all_sess)

new_l = np.empty([lines.shape[0], 5], dtype=object)

for i in range(lines.shape[0]):
    l = np.array(lines[i])    
    if lines[i][0] in dict_multiples:
        y = np.array(dict_multiples.get(lines[i][0]))
        new_l[i] = np.append(l, y)
    else:
        y = np.array(avg_duration)
        new_l[i] = np.append(l, y)

with open('p3-feature.csv', 'w') as out:
    for i in range(new_l.shape[0]):
        t = iso8601.parse_date(new_l[i][1])
        new_l[i][1] = t
        data = new_l[i].tolist()
        writer = csv.writer(out)
        writer.writerow(data)

