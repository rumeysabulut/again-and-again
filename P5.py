
# coding: utf-8

# In[1]:


import sys
import json
import time
import os
import numpy as np
import pandas as pd
import math
import random
import csv
import iso8601
import datetime


# In[2]:


f = path = "/Users/rumeysabulut/Documents/Bitirme/yoochoose-data" + "/only-multiples.csv"
pf = pd.read_csv(filepath_or_buffer = f, header=None, sep=',') 
lines_multi = pf.iloc[:,0:2].values


# In[3]:


dict_multiples = {}

for i in range(lines_multi.shape[0]):    
    inp = lines_multi[i][0]
    val = iso8601.parse_date(lines_multi[i][1])
    if inp in dict_multiples:
        dict_multiples[inp].append(val)
    else:
        dict_multiples[inp] = [val]


# In[6]:


for key in dict_multiples:
    time_list = []
    time_list = dict_multiples.get(key)
    time_list.sort()
    diff = []
    for i in range(len(time_list)):
        if i+1 != len(time_list):
            d = time_list[i+1] - time_list[i]
            diff.append(d)
    total_val = datetime.datetime.now();
    total_val -= total_val;
    for j in range(len(diff)):
        total_val += diff[j]
    
    avg_bet_clicks = total_val/len(time_list)
    dict_multiples[key] = avg_bet_clicks


# In[7]:


f1 = path = "/Users/rumeysabulut/Documents/Bitirme/yoochoose-data" + "/p3-p4-feature.csv"
df = pd.read_csv(filepath_or_buffer = f1, header=None, sep=',') 
lines = df.iloc[:,0:6].values

#to compute average value for one-clicked sessions
#I need the number of all sessions

all_sess = {}
for i in range(lines.shape[0]):
    if lines[i][0] not in all_sess:
        all_sess[lines[i][0]] = i


# In[8]:


total = datetime.datetime.now();
total -= total;
for key in dict_multiples:
    total += dict_multiples.get(key)

avg_bet_clicks = total / len(all_sess)


# In[10]:


avg_bet_cl = np.empty([lines.shape[0], 7], dtype=object)

for i in range(lines.shape[0]):
    l = np.array(lines[i])    
    if lines[i][0] in dict_multiples:
        y = np.array(dict_multiples.get(lines[i][0]))
        avg_bet_cl[i] = np.append(l, y)
    else:
        y = np.array(avg_bet_clicks)
        avg_bet_cl[i] = np.append(l, y)


# In[11]:


with open('p3-p4-p5-feature.csv', 'w') as out:
    for i in range(avg_bet_cl.shape[0]):
        data = avg_bet_cl[i].tolist()
        writer = csv.writer(out)
        writer.writerow(data)

