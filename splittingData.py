
# coding: utf-8

# In[2]:


import sys
import json
import time
import os
import numpy as np
import pandas as pd
import math
import random
import csv


# In[6]:


#Untitled1'deki read_buys_from_file'ın aynısı
def buys_dict(path):
    file = path + '/unique-buy.csv'
    df = pd.read_csv(filepath_or_buffer = file, header=None, sep=',') 
    lines = df.iloc[:,0:2].values 
    buy_session_dict = {} #dictionary to keep unique session IDs 
    c = 0 
    
    for i in range(lines.shape[0]): 
        if lines[i][0] not in buy_session_dict: 
            buy_session_dict[lines[i][0]] = c 
            c += 1
    print("read from file")
    return buy_session_dict


# In[24]:


def select_sess_from_clicks(path, buy_dict):
    file = path + "/yoochoose-clicks.dat"
    df = pd.read_csv(filepath_or_buffer = file, header=None, sep=',') 
    lines = df.iloc[:,0:4].values
    
    count = len(buy_dict)
    
    count_trbuy = round(count*0.7)

    count_evbuy = count - count_trbuy

    count_trnon = count_trbuy
    count_evnon = count_evbuy
    
    
    rand_num = random.sample(range(0, lines.shape[0]), lines.shape[0])
    rand_dict = {}
    for i in range(len(rand_num)):
        rand_dict[rand_num[i]] = i
    
    
    with open("train-all.csv", "a") as out1, open("eval-all.csv", "a") as out2:
        for key in rand_dict.items():
            data = lines[key[0]].tolist()

            if lines[key[0]][0] in buy_dict and count_trbuy > 0:
                writer1 = csv.writer(out1)
                writer1.writerow(data)
                count_trbuy -= 1
               
            elif lines[key[0]][0] in buy_dict and count_evbuy > 0:
                writer2 = csv.writer(out2)
                writer2.writerow(data)
                count_evbuy -= 1
                
            elif lines[key[0]][0] not in buy_dict and count_trnon > 0:
                writer3 = csv.writer(out1)
                writer3.writerow(data)
                count_trnon -= 1
                   
            elif lines[key[0]][0] not in buy_dict and count_evnon > 0:
                writer4 = csv.writer(out2)
                writer4.writerow(data)
                count_evnon -= 1
            


# In[ ]:


if __name__ == '__main__':
    path = "/Users/rumeysabulut/Documents/Bitirme/yoochoose-data"
    buy_dict = buys_dict(path)
    
    print(len(buy_dict))
    count = len(buy_dict)
    count_trbuy = round(count*0.7)
    print("total tr:", 2*count_trbuy)
    count_evbuy = count - count_trbuy
    print("total ev:", 2*count_evbuy)
    select_sess_from_clicks(path, buy_dict)

